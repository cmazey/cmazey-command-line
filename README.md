# cmazey-command-line
I have made this custom Command Line or REPL for fun, I hope you enjoy this!

<img alt="GitHub" src="https://img.shields.io/github/license/cmazey/cmazey-command-line?style=plastic"> <img alt="GitHub" src ="https://img.shields.io/badge/Made%20for-VSCode-informational?style=plastic"> <img alt="GitHub Repo stars" src="https://img.shields.io/github/stars/cmazey/cmazey-command-line?style=social"> <img alt="GitHub forks" src="https://img.shields.io/github/forks/cmazey/cmazey-command-line?style=social"> <img alt="Twitter URL" src="https://img.shields.io/twitter/url?style=social&url=https%3A%2F%2Fgithub.com%2Fcmazey%2Fcmazey-command-line"> ![Twitter Follow](https://img.shields.io/twitter/follow/cmazeyRBLX?style=social) <img alt="Website" src="https://img.shields.io/website?url=https%3A%2F%2Fcoltondogportraits.com%2F">

Click here to check out the [setup](https://github.com/cmazey/cmazey-command-line/tree/node/Cmd-REPL#---set-up---)!

> If you dont want to use the command line locally, then [click here](https://replit.com/@NotCmazey/cmazey-command-line?v=1) to use it without doing anything in the setup!
## COMMANDS:
------------------
- admin
- help
- crazy
- about
- clear
- sleepy

More coming soon!

### REQUIREMENTS
------------------
Make sure you have [NODE.JS](https://nodejs.org/en/) in your operating system.
> I recommend the LTS version.


